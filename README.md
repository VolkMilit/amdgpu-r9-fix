# AMDGPU R9 Fix

This Linux daemon, trying to fix MSI R9 fans spinning constantly on 50% even on height temperature. That can't be fixed by lm-sensors, so I write this simple daemon. 

Unfortunately MSI R9 270 has same problem on Windows, but I don't know if there a way to automatize that process.

### How does it works

Daemon checks for `/sys/class/hwmon/hwmon1/temp1_input` (note, your number may be different, daemon trying to find it) values every 10 seconds if value more then `45000` (by default, 45 celsius), `/sys/class/hwmon/hwmon1/pwm1_enable` will be set to `2` else set to `0`. This will force performance mode, which will make fans spin on 100%.

### OpenRC daemon

Daemon will be enabled automatically, if you're using Artix or other system with pacman support.

Enable:

```sh
rc-update add amdgpu-fix default
rc-service amdgpu-fix start
```

Disable:

```sh
rc-service amdgpu-fix stop
rc-update del amdgpu-fix default
```

### Altering settings

Change `temps.h` in `src/`.

### License

AMDGPU R9 Fix Copyright (C) 2021 Volk_Milit (aka Ja'Virr-Dar)

This program comes with ABSOLUTELY NO WARRANTY.

This is free software, and you are welcome to redistribute it under certain conditions.
