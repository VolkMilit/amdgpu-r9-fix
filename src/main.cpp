#include <iostream>
#include <memory>
#include <csignal>
#include <unistd.h>

#include "daemon.h"

using namespace std;

int main()
{
    if (getuid() != 0)
    {
        cerr << "This program must be run as root.\n";
        return -1;
    }

    unique_ptr<MainDaemon> daemon;

    signal(SIGINT, MainDaemon::stop);
    signal(SIGTERM, MainDaemon::stop);

    daemon->loop();

    return 0;
}
