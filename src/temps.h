#ifndef TEMPS_H
#define TEMPS_H

// Temps in celsius, when fans starting to spin on max
// For R9 max tems are 90+ degrees, but I advice keep it
// as low as possible (MSI has very bad heatsink, especially
// on memory modules)

// If your motherboard doesn't had a radiators on mosfets,
// then your max case tems are 25-30, otherwize degradetion
// is immenent

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

struct Temps
{    
    static inline int gpu_max_temp = 45;    

    static int get_state(const std::string &hwmon, const std::string &hwnum)
    {
        const std::string &hw = hwmon + "/pwm" + hwnum + "_enable";
        int ret;

        std::ifstream pwm;
        pwm.open(hw);

        std::stringstream buffer;
        buffer << pwm.rdbuf();
        buffer >> ret;

        pwm.close();

        return ret;
    }

    static int get_temp(const std::string &hwmon, const std::string &hwnum)
    {
        const std::string &hw = hwmon + "/temp" + hwnum + "_input";
        int ret;

        std::ifstream pwm;
        pwm.open(hw);

        std::stringstream buffer;
        buffer << pwm.rdbuf();
        buffer >> ret;

        pwm.close();

        // The temps returns in thousands, so we need to pass it in tens
        return (ret / 1000);
    }
};

#endif // TEMPS_H
