#include "daemon.h"

#include <iostream>
#include <filesystem>
#include <fstream>
#include <sstream>
#include <chrono>
#include <thread>

#include "temps.h"

using namespace filesystem;

MainDaemon::MainDaemon()
{
    started = true;

    for (const auto &path : directory_iterator("/sys/class/hwmon/"))
    {
        const string &name_path = path.path().string();

        if (path.is_directory() && exists(name_path + "/name"))
        {
            ifstream name;
            name.open(name_path + "/name");

            stringstream buffer;
            buffer << name.rdbuf();

            if (buffer.str() == "amdgpu\n")
            {
                m_amdgpu_path = name_path;
                break;
            }
        }
    }
}

MainDaemon::~MainDaemon()
{
    ofstream pwm_disable;
    pwm_disable.open(m_pwm_path);
    pwm_disable << "2\n";
}

void MainDaemon::set_pwm_state(const string &hwmon, const string &pwmnum, int state)
{
    m_pwm_path = hwmon + "/pwm" + pwmnum + "_enable";

    stringstream num_to_str;
    num_to_str << state;

    ofstream pwm_settle;
    pwm_settle.open(m_pwm_path);
    pwm_settle << num_to_str.str() + "\n";
}

void MainDaemon::loop()
{
    if (m_amdgpu_path.empty())
    {
        cerr << "No amdgpu installed. Nothing to control!\n";
        return;
    }

    while(started)
    {
        if (!m_amdgpu_path.empty())
        {
            const int temp = Temps::get_temp(m_amdgpu_path, "1");

            // We can't check, if state is chaged, gpu trying to change it back to 1 or 2 everytime
            if (m_gpu_temp != temp)
            {
                // Workaround bug
                // If we will changing this too frequntly, GPU will
                // hang sooner or later
                // This happens due to faulty hardware, but also,
                // maybe a bug in GPU BIOS or bug in AMDGPU
                // itself, I still can't figure this out, since
                // this issue is completly random
                if (temp > Temps::gpu_max_temp)
                    set_pwm_state(m_amdgpu_path, "1", 0);
                else
                    set_pwm_state(m_amdgpu_path, "1", 2);
            }

            m_gpu_temp = temp;
        }

        this_thread::sleep_for(chrono::seconds(10));
    }
}

void MainDaemon::stop(int)
{
    cerr << "Interrupt signal catched.\n";
    started = false;
}
