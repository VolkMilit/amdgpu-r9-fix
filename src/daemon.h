#ifndef DAEMON_H
#define DAEMON_H

#include <string>

using namespace std;

class MainDaemon
{
    public:
        MainDaemon();
        ~MainDaemon();

        void loop();
        static void stop(int);

    private:
        void set_pwm_state(const string &hwmon, const string &pwmnum, int state);

        string m_amdgpu_path;
        string m_pwm_path;

        int m_gpu_temp = 0;

        static inline bool started;
};

#endif // DAEMON_H
